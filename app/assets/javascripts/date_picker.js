﻿(function() {
    var  click_solution = true;
    var Date_picker_object = function() {

        this.init();
        this.Draw();

    }

    Date_picker_object.prototype.init = function() {

        this.node = document.getElementById('component_d_p_pattern_div');
        this.date_picker = document.getElementById('component_d_p_date_picker');
        this.cell_width = 42;
        this.cell_height = 42;
        this.date_picker.style.width = (this.cell_width+2)*7+'px';
        this.date_picker.style.height = (this.cell_height+2)*7+'px';
        this.cell_array = [];
        this.current_date = new Date();
        this.date = this.current_date.getDate();
        this.day = this.current_date.getDay();
        if (this.day == 0) this.day = 7;
        this.month = this.current_date.getMonth();
        this.month_day_number_array = new Array( 31 , 28 , 31 , 30 , 31 , 30 , 31 , 31 , 30 , 31 , 30 , 31 );
        this.month_names_array = new Array( 'Январь' , 'Февраль' , 'Март' , 'Апрель' , 'Май' , 'Июнь' , 'Июль' , 'Август' , 'Сентябрь' , 'Октябрь' , 'Ноябрь' , 'Декабрь');
        this.month_div_array = new Array( document.getElementById('component_d_p_date_picker_left_month') , document.getElementById('component_d_p_date_picker_center_month'), document.getElementById('component_d_p_date_picker_right_month'));
        this.start_day = ( ( parseInt( this.date / 7 ) + 1 ) * 7 ) - ( this.date + ( 7 - this.day ) );
        this.counter = 0;
        var dp = this;
        //  this.date = 23;
        // this.day = 5;

        for (var i = 0; i < this.month_div_array.length; i++) {

            this.month_div_array[i].style.color = '#ffffff';
            this.month_div_array[i].style.paddingTop = '8px';
            this.month_div_array[i].style.fontSize = '16px';
            this.month_div_array[i].style.fontFamily = 'Roboto';
            this.month_div_array[i].style.position = 'relative';
            this.month_div_array[i].style.display = 'inline-block';
            this.month_div_array[i].style.float = 'left';
            this.month_div_array[i].style.width = ((this.cell_width+2)*7/3-13)+'px';
            this.month_div_array[i].style.height = (this.cell_height-(this.cell_height/2-8)).toString()+'px';
        }


        this.month_div_array[2].style.float = 'right'
        this.month_div_array[1].style.margin = 'auto'

        this.month_div_array[0].innerText = this.month_names_array[this.month-1];
        this.month_div_array[0].style.textAlign = 'left';
        this.month_div_array[0].style.paddingLeft = '15px';
        this.month_div_array[0].style.textAlign = 'left';
        this.month_div_array[0].style.cursor = 'pointer';
        this.month_div_array[1].innerText = this.month_names_array[this.month];
        this.month_div_array[1].style.textAlign = 'center';
        this.month_div_array[1].style.cursor = 'default';
        this.month_div_array[2].innerText = this.month_names_array[this.month+1];
        this.month_div_array[2].style.textAlign = 'right';
        this.month_div_array[2].style.paddingRight = '15px';
        this.month_div_array[2].style.cursor = 'pointer';

        this.month_div_array[0].addEventListener('click',
            function () {
                dp.PrevMonth();
            });
        this.month_div_array[2].addEventListener('click',
            function () {
                dp.NextMonth();
            });



    }

    Date_picker_object.prototype.Draw = function() {

        var next_month_day = 1;
        var date_picker = this;
        for (var i = 0; i < 6; i++) {
            var buffer = [];
            for (var j = 0; j < 7; j++) {


                var new_node = this.node.cloneNode(true);
                new_node.removeAttribute('id');
                new_node.style.width = this.cell_width.toString()+'px';
                new_node.style.height = (this.cell_height-(this.cell_height/2-7)).toString()+'px';
                new_node.style.position = 'relative';
                new_node.style.display = 'inline-block';
                new_node.style.float = 'left';
                new_node.style.margin = '1px';
                new_node.style.paddingTop = (this.cell_height/2-7)+'px';


                var text_node = this.node.cloneNode(true);
                text_node.style.fontSize = '15px';
                text_node.style.fontFamily = 'Roboto';
                text_node.style.color = '#ffffff';
                text_node.style.textAlign = 'center';

                text_node.style.cursor = 'pointer';

                if ( this.counter >= this.start_day && this.month_day_number_array[this.month] >= ( this.counter - this.start_day + 1))  {

                    text_node.textContent = this.counter - this.start_day + 1;
                    new_node.style.backgroundColor = '#324864';
                    if ( j > 4 ) new_node.style.backgroundColor = 'rgb( 65, 90, 123 )';

                    new_node.addEventListener('mouseover' , this.cell_over);

                    new_node.addEventListener('mouseout' , this.cell_out);

                    new_node.addEventListener('click' , function() {
                        if (click_solution) {
                            click_solution = false;
                            date_picker.ChangeMonth(date_picker.start_day, date_picker.month ,  parseInt(this.textContent)  , date_picker.start_day);
                            setTimeout(function(){
                                click_solution = true;
                            } ,300)
                        }
                    });

                    new_node.style.opacity = 1;


                }
                else if (this.counter < this.start_day) {
                    text_node.textContent =  this.month_day_number_array[this.month-1] - (this.start_day - this.counter ) + 1;
                    new_node.style.backgroundColor = '#226195';
                    new_node.style.opacity = 0.5;
                }
                else {
                    text_node.textContent = next_month_day;
                    next_month_day++;
                    new_node.style.backgroundColor = '#226195';
                    new_node.style.opacity = 0.5;
                }
                this.counter++;


                new_node.appendChild(text_node);

                buffer.push(new_node);
            }
            this.cell_array.push(buffer);
        }

        this.cell_array[parseInt(this.date/7)][parseInt(this.day - 1)].style.backgroundColor = '#36b16a';
        this.cell_array[parseInt(this.date/7)][parseInt(this.day - 1)].style.color = '#36b16a';


        for (var i = 0; i < 6; i++)
            for (var j = 0; j < 7; j++) {
                this.date_picker.appendChild(this.cell_array[i][j]);

            };
    }

    Date_picker_object.prototype.NextMonth = function() {
//alert('dsfs')
        var curr_month = this.month;

        if (this.month == 11) this.month = 0;
        else this.month++;

        if (this.month == 0 ) this.month_div_array[0].innerText = this.month_names_array[11];
        else this.month_div_array[0].innerText = this.month_names_array[this.month-1];
        this.month_div_array[1].innerText = this.month_names_array[this.month];
        if (this.month == 11 ) this.month_div_array[2].innerText = this.month_names_array[0];
        else this.month_div_array[2].innerText = this.month_names_array[this.month+1];

        this.start_day = (this.month_day_number_array[curr_month] - 28) + this.start_day;
        if (this.start_day > 7 ) this.start_day -= 7;
        this.ChangeMonth(this.start_day, this.month , 1 , this.start_day);

    }

    Date_picker_object.prototype.PrevMonth = function() {

        if (this.month == 0) this.month = 11;
        else this.month--;

        if (this.month == 0 ) this.month_div_array[0].innerText = this.month_names_array[11];
        else this.month_div_array[0].innerText = this.month_names_array[this.month-1];
        this.month_div_array[1].innerText = this.month_names_array[this.month];
        if (this.month == 11 ) this.month_div_array[2].innerText = this.month_names_array[0];
        else this.month_div_array[2].innerText = this.month_names_array[this.month+1];

        this.start_day = 35 - this.month_day_number_array[this.month] + this.start_day;
        if (this.start_day > 7 ) this.start_day -= 7;
        this.ChangeMonth(this.start_day, this.month , 1 , this.start_day);
    }

    Date_picker_object.prototype.ChangeMonth = function(start_day , month , date , day) {

        var counter = 0;
        var next_month_day = 1;
        var date_picker = this;
        for (var i = 0; i < this.cell_array.length; i++) {
            for (var j = 0; j < 7; j++) {

                var new_node = this.cell_array[i][j];
                var text_node = new_node.firstElementChild;

                if ( counter >= start_day && this.month_day_number_array[month] >= (counter - start_day + 1))  {

                    text_node.textContent = counter - start_day + 1;
                    new_node.style.backgroundColor = '#324864';
                    if ( j > 4 ) new_node.style.backgroundColor = 'rgb( 65, 90, 123 )';

                    new_node.addEventListener('mouseover' , this.cell_over);

                    new_node.addEventListener('mouseout' , this.cell_out);

                    new_node.addEventListener('click' , function() {
                        if (click_solution) {
                            click_solution = false;
                            date_picker.ChangeMonth(date_picker.start_day, date_picker.month ,  parseInt(this.textContent)  , date_picker.start_day);
                            setTimeout(function(){
                                click_solution = true;
                            } ,300)
                        }
                    });

                    new_node.style.opacity = 1;

                    if ((counter - start_day + 1) == date ) {
                        new_node.style.backgroundColor = 'rgb(54, 177, 106)';
                        new_node.style.color = 'rgb(54, 177, 106)';
                    }
                }
                else if (counter < start_day) {
                    if ( this.month == 0 ) text_node.textContent =  this.month_day_number_array[11] - (start_day - counter ) + 1;
                    else text_node.textContent =  this.month_day_number_array[this.month-1] - (start_day - counter ) + 1;
                    new_node.style.backgroundColor = '#226195';
                    new_node.style.opacity = 0.5;
                }
                else {
                    text_node.textContent = next_month_day;
                    next_month_day++;
                    new_node.style.backgroundColor = '#226195';
                    new_node.style.opacity = 0.5;
                }
                counter++;


                new_node.appendChild(text_node);

            }
        }

    }

    Date_picker_object.prototype.cell_out = function(event) {

        this.style.backgroundColor = this.style.color;
    }

    Date_picker_object.prototype.cell_over = function(event) {
        this.style.color = this.style.backgroundColor;
        this.style.backgroundColor = '#3d679c';


    }

    return new Date_picker_object();
} ())