function show_image(object) {

    var img = document.getElementById('image_full').getElementsByTagName('img')[0];
    var fon = document.getElementById('fon');
    fon.style.display= 'block';
    img.style.display = 'none';
    img.setAttribute('src' , object.getAttribute('date_full_url'))
    document.getElementById('image_full').innerHTML = ''
    document.getElementById('image_full').appendChild(img);
    img.onload  = function() {
        if ( img.width > screen.width && img.height < screen.height-150) {

            resize_image(img , screen.width - 200, 0);
        }
        if ( img.width < screen.width && img.height > screen.height-150) {

            resize_image(img , 0 , screen.height-150);

        }
        if ( img.width > screen.width && img.height > screen.height-150) {

            if (img.width  < img.height) {

                resize_image(img , 0 , screen.height-150);
            }

            if (img.width  > img.height) {

                resize_image(img , screen.width-200 , 0);
            }
        }

        img.style.left = screen.width/2 - img.width/2 + 80 + 'px'
        img.style.top = screen.height/2 - img.height/2 + 'px'
        img.style.display = 'block';


        document.getElementsByTagName('body')[0].style.overflow = 'hidden'
    }

}

function hide_image() {
    var fon = document.getElementById('fon');
    fon.style.display= 'none';
    var img = document.getElementById('image_full').getElementsByTagName('img')[0];
    img.style.display='none';
    document.getElementsByTagName('body')[0].style.overflow = 'auto'

}

function resize_image(image ,width , height) {

    if ( width == 0 ) {

        image.style.width = (image.width/100)*(height / (image.height / 100))+'px';
        image.style.height = height+'px';
    }
    if ( height == 0 ) {

        image.style.height = (image.height/100)*(width / (image.width / 100))+'px';
        image.style.width =  width+'px';

    }

}