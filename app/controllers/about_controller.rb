class AboutController < ApplicationController
  def index
    @news = News.order(created_at: :desc).limit(6)

    @previews = Preview.order(created_at: :desc).limit(4)

    @quotations = Quotation.order(created_at: :desc).limit(4)

    @events = Event.order(created_at: :desc).limit(4)

    @services = ServiceList.all

  end

  def info

    @services = ServiceList.all

    @events = Event.order(created_at: :desc).limit(4)

    @content = StaticText.find_by_name('about_info')

  end

  def chamber_board
  end

  def chamber_guide
  end

  def chamber_structure
  end

  def layout_diagram
  end
end
