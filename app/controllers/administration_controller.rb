class AdministrationController < ApplicationController
  before_filter :authenticate_user!
  def index
    render :layout => nil
  end

  def localization
  end
end
