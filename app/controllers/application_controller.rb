class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_filter :get_labels

  def get_labels()

    @header_images = HeaderImage.all

    locals = Localization.all
    @labels = {}

    locals.each  do |loc|

      @labels[loc.name] = loc.ru

    end

    @left_bottom_menu = []
    @left_bottom_menu[0] = [ '/navigations/contacts' ,'Контакты' ]
    @left_bottom_menu[1] = [ '/navigations/albums' ,'Галлерея' ]
    @left_bottom_menu[2] = [ '/navigations/albums' ,'Комитеты ТПП РД' ]

  end
end
