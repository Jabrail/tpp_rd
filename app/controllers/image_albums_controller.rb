class ImageAlbumsController < ApplicationController
  before_action :set_image_album, only: [:show, :edit, :update, :destroy]

  # GET /image_albums
  # GET /image_albums.json
  def index
    @image_albums = ImageAlbum.all

    render :layout =>  "../administration/index"

  end

  # GET /image_albums/1
  # GET /image_albums/1.json
  def show

    puts params[:id]
    @image_galleries = ImageGallery.find_all_by_album_id(params[:id])


    render :layout =>  "../administration/index"

  end

  # GET /image_albums/new
  def new
    @image_album = ImageAlbum.new

    render :layout =>  "../administration/index"

  end

  # GET /image_albums/1/edit
  def edit

    render :layout =>  "../administration/index"

  end

  # POST /image_albums
  # POST /image_albums.json
  def create
    @image_album = ImageAlbum.new(image_album_params)

    respond_to do |format|
      if @image_album.save
        format.html { redirect_to @image_album, notice: 'Image album was successfully created.' }
        format.json { render action: 'show', status: :created, location: @image_album }
      else
        format.html { render action: 'new' }
        format.json { render json: @image_album.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /image_albums/1
  # PATCH/PUT /image_albums/1.json
  def update
    respond_to do |format|
      if @image_album.update(image_album_params)
        format.html { redirect_to @image_album, notice: 'Image album was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @image_album.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /image_albums/1
  # DELETE /image_albums/1.json
  def destroy
    @image_album.destroy
    respond_to do |format|
      format.html { redirect_to image_albums_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image_album
      @image_album = ImageAlbum.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_album_params
      params.require(:image_album).permit(:name, :description)
    end
end
