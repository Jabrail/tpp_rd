class ImageGalleriesController < ApplicationController
  before_action :set_image_gallery, only: [:show, :edit, :update, :destroy]

  # GET /image_galleries
  # GET /image_galleries.json
  def index
    @image_galleries = ImageGallery.all

    render :layout =>  "../administration/index"
  end

  # GET /image_galleries/1
  # GET /image_galleries/1.json
  def show
 
    render :layout =>  "../administration/index"

  end

  # GET /image_galleries/new
  def new
    @image_gallery = ImageGallery.new

    render :layout =>  "../administration/index"

  end

  # GET /image_galleries/1/edit
  def edit

    render :layout =>  "../administration/index"

  end

  # POST /image_galleries
  # POST /image_galleries.json
  def create
    @image_gallery = ImageGallery.new(image_gallery_params)

    respond_to do |format|
      if @image_gallery.save
        format.html { redirect_to @image_gallery, notice: 'Image gallery was successfully created.' }
        format.json { render action: 'show', status: :created, location: @image_gallery }
      else
        format.html { render action: 'new' }
        format.json { render json: @image_gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /image_galleries/1
  # PATCH/PUT /image_galleries/1.json
  def update
    respond_to do |format|
      if @image_gallery.update(image_gallery_params)
        format.html { redirect_to @image_gallery, notice: 'Image gallery was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @image_gallery.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /image_galleries/1
  # DELETE /image_galleries/1.json
  def destroy
    @image_gallery.destroy
    respond_to do |format|
      format.html { redirect_to image_galleries_url }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_image_gallery
      @image_gallery = ImageGallery.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def image_gallery_params
      params.require(:image_gallery).permit(:name, :description, :avatar, :album_id)
    end
end
