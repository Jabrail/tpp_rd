class NavigationsController < ApplicationController

  def news_show

    @news = News.find(params[:id])

    @services = ServiceList.all

  end

  def news

    if params[:offset]
      offset = params[:offset]
    else
      offset = 0
    end
    @news = News.order(created_at: :desc).limit(3).offset(offset)

    @news_length = News.all.length

    @services = ServiceList.all

  end

  def previews

    if params[:offset]
      offset = params[:offset]
    else
      offset = 0
    end
    @previews = Preview.order(created_at: :desc).limit(3).offset(offset)

    @previews_length = Preview.all.length

    @services = ServiceList.all

  end


  def preview_show

    @preview = Preview.find(params[:id])

    @services = ServiceList.all

  end

  def event_show

    @event = Event.find(params[:id])

    @services = ServiceList.all

  end

  def events

    if params[:offset]
      offset = params[:offset]
    else
      offset = 0
    end
    @events = Event.order(created_at: :desc).limit(3).offset(offset)

    @event_length = Event.all.length

    @services = ServiceList.all

  end

  def quotations_show

    @quotation = Quotation.find(params[:id])

    @services = ServiceList.all

  end

  def quotations

    if params[:offset]
      offset = params[:offset]
    else
      offset = 0
    end
    @quotations = Quotation.order(created_at: :desc).limit(3).offset(offset)

    @quotations_length = Quotation.all.length

    @services = ServiceList.all

  end

  def gallery

    if params[:offset]
      offset = params[:offset]
    else
      offset = 0
    end

    @services = ServiceList.all

    @images_length = ImageGallery.all.length

    @images = ImageGallery.find_all_by_album_id(params[:album_id] , :limit => '12' , :offset => offset)

  end

  def albums

    @albums = ImageAlbum.all


    @services = ServiceList.all

  end
end
