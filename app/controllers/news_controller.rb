class NewsController < ApplicationController
  before_filter :authenticate_user!
  def index
    @news = News.order(created_at: :desc)

    render :layout =>  "../administration/index"

  end

  def show
   @news = News.find(params[:id])

   render :layout =>  "../administration/index"
  end

  def new
    @news = News.new

    render :layout =>  "../administration/index"

  end

  def create
    @news = News.new(news_params)

    respond_to do |format|
      if @news.save
        format.html { redirect_to @news, notice: 'My news was successfully created.' }
        format.json { render json: @news, status: :created, location: @my_price }
      else
        format.html { render action: "new" }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
      end
  end

  def edit
    @news = News.find(params[:id])

    render :layout =>  "../administration/index"

  end

  def update
    @news = News.new(news_params)

    respond_to do |format|
      if @news.save
        format.html { redirect_to @news, notice: 'My news was successfully created.' }
        format.json { render json: @news, status: :created, location: @my_price }
      else
        format.html { render action: "new" }
        format.json { render json: @news.errors, status: :unprocessable_entity }
      end
    end

  end

  def destroy
    @news = News.find(params[:id])
    @news.destroy

    respond_to do |format|
      format.html { redirect_to news_index_path }
      format.json { head :no_content }
    end
  end

  private
  def news_params
      params.require(:news).permit( :title, :data, :small_text, :text, :seo_title, :seo_keywords, :seo_description )
  end
end
