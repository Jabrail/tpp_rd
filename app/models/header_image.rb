class HeaderImage < ActiveRecord::Base

  do_not_validate_attachment_file_type :avatar
  has_attached_file :avatar, :styles => { :medium => "1076x274>", :thumb => "100x100>" }

end
