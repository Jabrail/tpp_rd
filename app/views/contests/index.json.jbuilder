json.array!(@contests) do |contest|
  json.extract! contest, :id, :title, :small_text, :text
  json.url contest_url(contest, format: :json)
end
