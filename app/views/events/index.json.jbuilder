json.array!(@events) do |event|
  json.extract! event, :id, :title, :small_text, :text, :date, :lat, :lon
  json.url event_url(event, format: :json)
end
