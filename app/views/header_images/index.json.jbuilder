json.array!(@header_images) do |header_image|
  json.extract! header_image, :id
  json.url header_image_url(header_image, format: :json)
end
