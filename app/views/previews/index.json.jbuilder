json.array!(@previews) do |preview|
  json.extract! preview, :id, :title, :date, :text, :small_text
  json.url preview_url(preview, format: :json)
end
