json.array!(@quotations) do |quotation|
  json.extract! quotation, :id, :title, :date, :text
  json.url quotation_url(quotation, format: :json)
end
