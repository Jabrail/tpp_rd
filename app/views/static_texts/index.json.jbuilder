json.array!(@static_texts) do |static_text|
  json.extract! static_text, :id, :name, :text
  json.url static_text_url(static_text, format: :json)
end
