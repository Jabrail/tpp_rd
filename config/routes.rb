Tpprd::Application.routes.draw do


  resources :header_images

  resources :hrader_images

  resources :quotations

  resources :image_albums

  resources :image_galleries

  post '/rate' => 'rater#create', :as => 'rate'
  resources :service_lists

  get "navigations/news_show"
  get "navigations/news"
  get "navigations/gallery"
  get "navigations/albums"
  get "navigations/previews"
  get "navigations/preview_show"
  get "navigations/events"
  get "navigations/event_show"
  get "navigations/quotations"
  get "navigations/quotations_show"

=begin
  get "service/f_t_contracts"
  get "service/b_e_a_t"
  get "service/e_i_c_c"
  get "service/i_and_i"
  get "service/mediation"
  get "service/org_department"
  get "service/translations"
  get "service/legal_support"
  get "service/arbitral_tribunal"
=end
  resources :static_texts


  resources :service

  devise_for :users

  resources :contests


  resources :departments

  resources :people

  resources :events

  resources :previews

  get "about/index"
  get "about/info"


  get "administration/index"
  get "administration/localization"
  get "members_tpp/how_become"
  get "members_tpp/docs"
  resources :news
  mount Ckeditor::Engine => '/ckeditor'

  get "service/f_e_activity"

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
   root 'about#index'

   get 'administration/index', as: 'user_root'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
