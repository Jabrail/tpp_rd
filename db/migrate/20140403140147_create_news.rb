class CreateNews < ActiveRecord::Migration
  def change
    create_table :news do |t|
      t.text :title, :limit => 512
      t.date :data
      t.text :small_text, :limit => 512
      t.text :text
      t.text :seo_title, :limit => 512
      t.text :seo_keywords, :limit => 512
      t.text :seo_description

      t.timestamps
    end
  end
end
