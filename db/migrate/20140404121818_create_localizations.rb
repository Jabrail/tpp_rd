class CreateLocalizations < ActiveRecord::Migration
  def change
    create_table :localizations do |t|
      t.string :name
      t.text :ru
      t.text :eng

      t.timestamps
    end
  end
end
