class CreatePreviews < ActiveRecord::Migration
  def change
    create_table :previews do |t|
      t.string :title
      t.date :date
      t.text :text
      t.string :small_text

      t.timestamps
    end
  end
end
