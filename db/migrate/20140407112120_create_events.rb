class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.string :title
      t.string :small_text
      t.text :text
      t.date :date
      t.string :lat
      t.string :lon

      t.timestamps
    end
  end
end
