class CreateDepartments < ActiveRecord::Migration
  def change
    create_table :departments do |t|
      t.string :name
      t.integer :head_id
      t.text :description
      t.text :contacts

      t.timestamps
    end
  end
end
