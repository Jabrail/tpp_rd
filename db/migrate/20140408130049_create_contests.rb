class CreateContests < ActiveRecord::Migration
  def change
    create_table :contests do |t|
      t.string :title
      t.string :small_text
      t.text :text

      t.timestamps
    end
  end
end
