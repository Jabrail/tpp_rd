class AddAttachmentAvatarToContests < ActiveRecord::Migration
  def self.up
    change_table :contests do |t|
      t.attachment :avatar
    end
  end

  def self.down
    drop_attached_file :contests, :avatar
  end
end
