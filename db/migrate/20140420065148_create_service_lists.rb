class CreateServiceLists < ActiveRecord::Migration
  def change
    create_table :service_lists do |t|
      t.string :name
      t.string :service_href

      t.timestamps
    end
  end
end
