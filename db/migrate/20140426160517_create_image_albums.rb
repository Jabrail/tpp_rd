class CreateImageAlbums < ActiveRecord::Migration
  def change
    create_table :image_albums do |t|
      t.string :name
      t.string :description

      t.timestamps
    end
  end
end
