class CreateQuotations < ActiveRecord::Migration
  def change
    create_table :quotations do |t|
      t.string :title
      t.date :date
      t.text :text

      t.timestamps
    end
  end
end
