class AddAvatarColumnToHeaderImages < ActiveRecord::Migration
  def self.up
    change_table :header_images do |t|
      t.has_attached_file :avatar
    end
  end

  def self.down
    drop_attached_file :header_images, :avatar
  end
end
