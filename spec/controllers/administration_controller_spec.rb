require 'spec_helper'

describe AdministrationController do

  describe "GET 'index'" do
    it "returns http success" do
      get 'index'
      response.should be_success
    end
  end

  describe "GET 'localization'" do
    it "returns http success" do
      get 'localization'
      response.should be_success
    end
  end

end
