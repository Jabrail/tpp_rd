require 'spec_helper'

describe ServiceController do

  describe "GET 'f_e_activiti'" do
    it "returns http success" do
      get 'f_e_activiti'
      response.should be_success
    end
  end

  describe "GET 'f_t_contracts'" do
    it "returns http success" do
      get 'f_t_contracts'
      response.should be_success
    end
  end

  describe "GET 'b_e_a_t'" do
    it "returns http success" do
      get 'b_e_a_t'
      response.should be_success
    end
  end

  describe "GET 'e_i_c_c'" do
    it "returns http success" do
      get 'e_i_c_c'
      response.should be_success
    end
  end

  describe "GET 'i_and_i'" do
    it "returns http success" do
      get 'i_and_i'
      response.should be_success
    end
  end

  describe "GET 'mediation'" do
    it "returns http success" do
      get 'mediation'
      response.should be_success
    end
  end

  describe "GET 'org_department'" do
    it "returns http success" do
      get 'org_department'
      response.should be_success
    end
  end

  describe "GET 'translations'" do
    it "returns http success" do
      get 'translations'
      response.should be_success
    end
  end

  describe "GET 'legal_support'" do
    it "returns http success" do
      get 'legal_support'
      response.should be_success
    end
  end

  describe "GET 'arbitral_tribunal'" do
    it "returns http success" do
      get 'arbitral_tribunal'
      response.should be_success
    end
  end

end
