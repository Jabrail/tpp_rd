require "spec_helper"

describe HeaderImagesController do
  describe "routing" do

    it "routes to #index" do
      get("/header_images").should route_to("header_images#index")
    end

    it "routes to #new" do
      get("/header_images/new").should route_to("header_images#new")
    end

    it "routes to #show" do
      get("/header_images/1").should route_to("header_images#show", :id => "1")
    end

    it "routes to #edit" do
      get("/header_images/1/edit").should route_to("header_images#edit", :id => "1")
    end

    it "routes to #create" do
      post("/header_images").should route_to("header_images#create")
    end

    it "routes to #update" do
      put("/header_images/1").should route_to("header_images#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/header_images/1").should route_to("header_images#destroy", :id => "1")
    end

  end
end
