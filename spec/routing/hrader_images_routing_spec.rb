require "spec_helper"

describe HraderImagesController do
  describe "routing" do

    it "routes to #index" do
      get("/hrader_images").should route_to("hrader_images#index")
    end

    it "routes to #new" do
      get("/hrader_images/new").should route_to("hrader_images#new")
    end

    it "routes to #show" do
      get("/hrader_images/1").should route_to("hrader_images#show", :id => "1")
    end

    it "routes to #edit" do
      get("/hrader_images/1/edit").should route_to("hrader_images#edit", :id => "1")
    end

    it "routes to #create" do
      post("/hrader_images").should route_to("hrader_images#create")
    end

    it "routes to #update" do
      put("/hrader_images/1").should route_to("hrader_images#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/hrader_images/1").should route_to("hrader_images#destroy", :id => "1")
    end

  end
end
