require "spec_helper"

describe ImageAlbumsController do
  describe "routing" do

    it "routes to #index" do
      get("/image_albums").should route_to("image_albums#index")
    end

    it "routes to #new" do
      get("/image_albums/new").should route_to("image_albums#new")
    end

    it "routes to #show" do
      get("/image_albums/1").should route_to("image_albums#show", :id => "1")
    end

    it "routes to #edit" do
      get("/image_albums/1/edit").should route_to("image_albums#edit", :id => "1")
    end

    it "routes to #create" do
      post("/image_albums").should route_to("image_albums#create")
    end

    it "routes to #update" do
      put("/image_albums/1").should route_to("image_albums#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/image_albums/1").should route_to("image_albums#destroy", :id => "1")
    end

  end
end
