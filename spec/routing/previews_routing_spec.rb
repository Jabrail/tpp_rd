require "spec_helper"

describe PreviewsController do
  describe "routing" do

    it "routes to #index" do
      get("/previews").should route_to("previews#index")
    end

    it "routes to #new" do
      get("/previews/new").should route_to("previews#new")
    end

    it "routes to #show" do
      get("/previews/1").should route_to("previews#show", :id => "1")
    end

    it "routes to #edit" do
      get("/previews/1/edit").should route_to("previews#edit", :id => "1")
    end

    it "routes to #create" do
      post("/previews").should route_to("previews#create")
    end

    it "routes to #update" do
      put("/previews/1").should route_to("previews#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/previews/1").should route_to("previews#destroy", :id => "1")
    end

  end
end
