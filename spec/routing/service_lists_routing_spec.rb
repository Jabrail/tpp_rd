require "spec_helper"

describe ServiceListsController do
  describe "routing" do

    it "routes to #index" do
      get("/service_lists").should route_to("service_lists#index")
    end

    it "routes to #new" do
      get("/service_lists/new").should route_to("service_lists#new")
    end

    it "routes to #show" do
      get("/service_lists/1").should route_to("service_lists#show", :id => "1")
    end

    it "routes to #edit" do
      get("/service_lists/1/edit").should route_to("service_lists#edit", :id => "1")
    end

    it "routes to #create" do
      post("/service_lists").should route_to("service_lists#create")
    end

    it "routes to #update" do
      put("/service_lists/1").should route_to("service_lists#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/service_lists/1").should route_to("service_lists#destroy", :id => "1")
    end

  end
end
