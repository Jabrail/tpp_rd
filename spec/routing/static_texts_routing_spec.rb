require "spec_helper"

describe StaticTextsController do
  describe "routing" do

    it "routes to #index" do
      get("/static_texts").should route_to("static_texts#index")
    end

    it "routes to #new" do
      get("/static_texts/new").should route_to("static_texts#new")
    end

    it "routes to #show" do
      get("/static_texts/1").should route_to("static_texts#show", :id => "1")
    end

    it "routes to #edit" do
      get("/static_texts/1/edit").should route_to("static_texts#edit", :id => "1")
    end

    it "routes to #create" do
      post("/static_texts").should route_to("static_texts#create")
    end

    it "routes to #update" do
      put("/static_texts/1").should route_to("static_texts#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/static_texts/1").should route_to("static_texts#destroy", :id => "1")
    end

  end
end
