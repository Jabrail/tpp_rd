require 'spec_helper'

describe "contests/edit" do
  before(:each) do
    @contest = assign(:contest, stub_model(Contest,
      :title => "MyString",
      :small_text => "MyString",
      :text => "MyText"
    ))
  end

  it "renders the edit contest form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", contest_path(@contest), "post" do
      assert_select "input#contest_title[name=?]", "contest[title]"
      assert_select "input#contest_small_text[name=?]", "contest[small_text]"
      assert_select "textarea#contest_text[name=?]", "contest[text]"
    end
  end
end
