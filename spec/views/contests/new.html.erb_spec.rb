require 'spec_helper'

describe "contests/new" do
  before(:each) do
    assign(:contest, stub_model(Contest,
      :title => "MyString",
      :small_text => "MyString",
      :text => "MyText"
    ).as_new_record)
  end

  it "renders new contest form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", contests_path, "post" do
      assert_select "input#contest_title[name=?]", "contest[title]"
      assert_select "input#contest_small_text[name=?]", "contest[small_text]"
      assert_select "textarea#contest_text[name=?]", "contest[text]"
    end
  end
end
