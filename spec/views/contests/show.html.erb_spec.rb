require 'spec_helper'

describe "contests/show" do
  before(:each) do
    @contest = assign(:contest, stub_model(Contest,
      :title => "Title",
      :small_text => "Small Text",
      :text => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Title/)
    rendered.should match(/Small Text/)
    rendered.should match(/MyText/)
  end
end
