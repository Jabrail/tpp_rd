require 'spec_helper'

describe "departments/edit" do
  before(:each) do
    @department = assign(:department, stub_model(Department,
      :name => "MyString",
      :head_id => 1,
      :description => "MyText",
      :contacts => "MyText"
    ))
  end

  it "renders the edit department form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", department_path(@department), "post" do
      assert_select "input#department_name[name=?]", "department[name]"
      assert_select "input#department_head_id[name=?]", "department[head_id]"
      assert_select "textarea#department_description[name=?]", "department[description]"
      assert_select "textarea#department_contacts[name=?]", "department[contacts]"
    end
  end
end
