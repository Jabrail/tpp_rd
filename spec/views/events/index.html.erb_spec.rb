require 'spec_helper'

describe "events/index" do
  before(:each) do
    assign(:events, [
      stub_model(Event,
        :title => "Title",
        :small_text => "Small Text",
        :text => "MyText",
        :lat => "Lat",
        :lon => "Lon"
      ),
      stub_model(Event,
        :title => "Title",
        :small_text => "Small Text",
        :text => "MyText",
        :lat => "Lat",
        :lon => "Lon"
      )
    ])
  end

  it "renders a list of events" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "Small Text".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Lat".to_s, :count => 2
    assert_select "tr>td", :text => "Lon".to_s, :count => 2
  end
end
