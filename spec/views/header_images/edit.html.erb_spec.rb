require 'spec_helper'

describe "header_images/edit" do
  before(:each) do
    @header_image = assign(:header_image, stub_model(HeaderImage))
  end

  it "renders the edit header_image form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", header_image_path(@header_image), "post" do
    end
  end
end
