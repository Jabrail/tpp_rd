require 'spec_helper'

describe "header_images/new" do
  before(:each) do
    assign(:header_image, stub_model(HeaderImage).as_new_record)
  end

  it "renders new header_image form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", header_images_path, "post" do
    end
  end
end
