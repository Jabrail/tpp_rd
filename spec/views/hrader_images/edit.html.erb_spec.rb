require 'spec_helper'

describe "hrader_images/edit" do
  before(:each) do
    @hrader_image = assign(:hrader_image, stub_model(HraderImage))
  end

  it "renders the edit hrader_image form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", hrader_image_path(@hrader_image), "post" do
    end
  end
end
