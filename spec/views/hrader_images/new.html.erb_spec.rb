require 'spec_helper'

describe "hrader_images/new" do
  before(:each) do
    assign(:hrader_image, stub_model(HraderImage).as_new_record)
  end

  it "renders new hrader_image form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", hrader_images_path, "post" do
    end
  end
end
