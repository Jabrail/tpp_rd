require 'spec_helper'

describe "image_albums/edit" do
  before(:each) do
    @image_album = assign(:image_album, stub_model(ImageAlbum,
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit image_album form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", image_album_path(@image_album), "post" do
      assert_select "input#image_album_name[name=?]", "image_album[name]"
      assert_select "input#image_album_description[name=?]", "image_album[description]"
    end
  end
end
