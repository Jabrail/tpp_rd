require 'spec_helper'

describe "image_albums/new" do
  before(:each) do
    assign(:image_album, stub_model(ImageAlbum,
      :name => "MyString",
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new image_album form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", image_albums_path, "post" do
      assert_select "input#image_album_name[name=?]", "image_album[name]"
      assert_select "input#image_album_description[name=?]", "image_album[description]"
    end
  end
end
