require 'spec_helper'

describe "image_albums/show" do
  before(:each) do
    @image_album = assign(:image_album, stub_model(ImageAlbum,
      :name => "Name",
      :description => "Description"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Description/)
  end
end
