require 'spec_helper'

describe "image_galleries/edit" do
  before(:each) do
    @image_gallery = assign(:image_gallery, stub_model(ImageGallery,
      :name => "MyString",
      :description => "MyString"
    ))
  end

  it "renders the edit image_gallery form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", image_gallery_path(@image_gallery), "post" do
      assert_select "input#image_gallery_name[name=?]", "image_gallery[name]"
      assert_select "input#image_gallery_description[name=?]", "image_gallery[description]"
    end
  end
end
