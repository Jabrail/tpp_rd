require 'spec_helper'

describe "image_galleries/new" do
  before(:each) do
    assign(:image_gallery, stub_model(ImageGallery,
      :name => "MyString",
      :description => "MyString"
    ).as_new_record)
  end

  it "renders new image_gallery form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", image_galleries_path, "post" do
      assert_select "input#image_gallery_name[name=?]", "image_gallery[name]"
      assert_select "input#image_gallery_description[name=?]", "image_gallery[description]"
    end
  end
end
