require 'spec_helper'

describe "people/edit" do
  before(:each) do
    @person = assign(:person, stub_model(Person,
      :name => "MyString",
      :last_name => "MyString",
      :patronymic => "MyString",
      :description => "MyText"
    ))
  end

  it "renders the edit person form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", person_path(@person), "post" do
      assert_select "input#person_name[name=?]", "person[name]"
      assert_select "input#person_last_name[name=?]", "person[last_name]"
      assert_select "input#person_patronymic[name=?]", "person[patronymic]"
      assert_select "textarea#person_description[name=?]", "person[description]"
    end
  end
end
