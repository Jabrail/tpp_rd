require 'spec_helper'

describe "people/new" do
  before(:each) do
    assign(:person, stub_model(Person,
      :name => "MyString",
      :last_name => "MyString",
      :patronymic => "MyString",
      :description => "MyText"
    ).as_new_record)
  end

  it "renders new person form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", people_path, "post" do
      assert_select "input#person_name[name=?]", "person[name]"
      assert_select "input#person_last_name[name=?]", "person[last_name]"
      assert_select "input#person_patronymic[name=?]", "person[patronymic]"
      assert_select "textarea#person_description[name=?]", "person[description]"
    end
  end
end
