require 'spec_helper'

describe "people/show" do
  before(:each) do
    @person = assign(:person, stub_model(Person,
      :name => "Name",
      :last_name => "Last Name",
      :patronymic => "Patronymic",
      :description => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Last Name/)
    rendered.should match(/Patronymic/)
    rendered.should match(/MyText/)
  end
end
