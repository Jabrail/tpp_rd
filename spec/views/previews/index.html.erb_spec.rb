require 'spec_helper'

describe "previews/index" do
  before(:each) do
    assign(:previews, [
      stub_model(Preview,
        :title => "Title",
        :text => "MyText",
        :small_text => "Small Text"
      ),
      stub_model(Preview,
        :title => "Title",
        :text => "MyText",
        :small_text => "Small Text"
      )
    ])
  end

  it "renders a list of previews" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Title".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
    assert_select "tr>td", :text => "Small Text".to_s, :count => 2
  end
end
