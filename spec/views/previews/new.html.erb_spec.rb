require 'spec_helper'

describe "previews/new" do
  before(:each) do
    assign(:preview, stub_model(Preview,
      :title => "MyString",
      :text => "MyText",
      :small_text => "MyString"
    ).as_new_record)
  end

  it "renders new preview form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", previews_path, "post" do
      assert_select "input#preview_title[name=?]", "preview[title]"
      assert_select "textarea#preview_text[name=?]", "preview[text]"
      assert_select "input#preview_small_text[name=?]", "preview[small_text]"
    end
  end
end
