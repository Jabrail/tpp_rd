require 'spec_helper'

describe "previews/show" do
  before(:each) do
    @preview = assign(:preview, stub_model(Preview,
      :title => "Title",
      :text => "MyText",
      :small_text => "Small Text"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Title/)
    rendered.should match(/MyText/)
    rendered.should match(/Small Text/)
  end
end
