require 'spec_helper'

describe "quotations/edit" do
  before(:each) do
    @quotation = assign(:quotation, stub_model(Quotation,
      :title => "MyString",
      :text => "MyText"
    ))
  end

  it "renders the edit quotation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", quotation_path(@quotation), "post" do
      assert_select "input#quotation_title[name=?]", "quotation[title]"
      assert_select "textarea#quotation_text[name=?]", "quotation[text]"
    end
  end
end
