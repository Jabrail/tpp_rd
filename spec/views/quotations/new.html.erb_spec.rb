require 'spec_helper'

describe "quotations/new" do
  before(:each) do
    assign(:quotation, stub_model(Quotation,
      :title => "MyString",
      :text => "MyText"
    ).as_new_record)
  end

  it "renders new quotation form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", quotations_path, "post" do
      assert_select "input#quotation_title[name=?]", "quotation[title]"
      assert_select "textarea#quotation_text[name=?]", "quotation[text]"
    end
  end
end
