require 'spec_helper'

describe "service_lists/edit" do
  before(:each) do
    @service_list = assign(:service_list, stub_model(ServiceList,
      :name => "MyString",
      :service_href => "MyString"
    ))
  end

  it "renders the edit service_list form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", service_list_path(@service_list), "post" do
      assert_select "input#service_list_name[name=?]", "service_list[name]"
      assert_select "input#service_list_service_href[name=?]", "service_list[service_href]"
    end
  end
end
