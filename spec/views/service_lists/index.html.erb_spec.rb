require 'spec_helper'

describe "service_lists/index" do
  before(:each) do
    assign(:service_lists, [
      stub_model(ServiceList,
        :name => "Name",
        :service_href => "Service Href"
      ),
      stub_model(ServiceList,
        :name => "Name",
        :service_href => "Service Href"
      )
    ])
  end

  it "renders a list of service_lists" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "Service Href".to_s, :count => 2
  end
end
