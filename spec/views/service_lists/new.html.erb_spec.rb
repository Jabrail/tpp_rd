require 'spec_helper'

describe "service_lists/new" do
  before(:each) do
    assign(:service_list, stub_model(ServiceList,
      :name => "MyString",
      :service_href => "MyString"
    ).as_new_record)
  end

  it "renders new service_list form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", service_lists_path, "post" do
      assert_select "input#service_list_name[name=?]", "service_list[name]"
      assert_select "input#service_list_service_href[name=?]", "service_list[service_href]"
    end
  end
end
