require 'spec_helper'

describe "service_lists/show" do
  before(:each) do
    @service_list = assign(:service_list, stub_model(ServiceList,
      :name => "Name",
      :service_href => "Service Href"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/Service Href/)
  end
end
