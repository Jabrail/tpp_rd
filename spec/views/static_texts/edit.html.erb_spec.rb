require 'spec_helper'

describe "static_texts/edit" do
  before(:each) do
    @static_text = assign(:static_text, stub_model(StaticText,
      :name => "MyString",
      :text => "MyText"
    ))
  end

  it "renders the edit static_text form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", static_text_path(@static_text), "post" do
      assert_select "input#static_text_name[name=?]", "static_text[name]"
      assert_select "textarea#static_text_text[name=?]", "static_text[text]"
    end
  end
end
