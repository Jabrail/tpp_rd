require 'spec_helper'

describe "static_texts/index" do
  before(:each) do
    assign(:static_texts, [
      stub_model(StaticText,
        :name => "Name",
        :text => "MyText"
      ),
      stub_model(StaticText,
        :name => "Name",
        :text => "MyText"
      )
    ])
  end

  it "renders a list of static_texts" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "tr>td", :text => "Name".to_s, :count => 2
    assert_select "tr>td", :text => "MyText".to_s, :count => 2
  end
end
