require 'spec_helper'

describe "static_texts/new" do
  before(:each) do
    assign(:static_text, stub_model(StaticText,
      :name => "MyString",
      :text => "MyText"
    ).as_new_record)
  end

  it "renders new static_text form" do
    render

    # Run the generator again with the --webrat flag if you want to use webrat matchers
    assert_select "form[action=?][method=?]", static_texts_path, "post" do
      assert_select "input#static_text_name[name=?]", "static_text[name]"
      assert_select "textarea#static_text_text[name=?]", "static_text[text]"
    end
  end
end
