require 'spec_helper'

describe "static_texts/show" do
  before(:each) do
    @static_text = assign(:static_text, stub_model(StaticText,
      :name => "Name",
      :text => "MyText"
    ))
  end

  it "renders attributes in <p>" do
    render
    # Run the generator again with the --webrat flag if you want to use webrat matchers
    rendered.should match(/Name/)
    rendered.should match(/MyText/)
  end
end
